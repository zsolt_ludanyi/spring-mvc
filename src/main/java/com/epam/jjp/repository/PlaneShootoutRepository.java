package com.epam.jjp.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.epam.jjp.domain.City;
import com.epam.jjp.domain.Plane;
import com.epam.jjp.domain.Plane.PlaneType;
import com.epam.jjp.domain.Route;

@Repository
public class PlaneShootoutRepository {
	private final List<City> cities = new ArrayList<>();
	private final List<Plane> planes = new ArrayList<>();
	private final List<Route> routes = new ArrayList<>();
	{
		City budapest = new City();
		budapest.setName("Budapest");
		Map<PlaneType, Integer> firePower = new HashMap<>();
		firePower.put(PlaneType.CHARITY, 10);
		firePower.put(PlaneType.CIVIL, 15);
		firePower.put(PlaneType.MILITARY, 20);
		budapest.setFirePowers(firePower);
		Map<PlaneType, Integer> likelihoods = new HashMap<>();
		likelihoods.put(PlaneType.CHARITY, 10);
		likelihoods.put(PlaneType.CIVIL, 15);
		likelihoods.put(PlaneType.MILITARY, 20);
		budapest.setLikelihoods(likelihoods);
		City becs = new City();
		becs.setName("Becs");
		becs.setLikelihoods(likelihoods);
		becs.setFirePowers(firePower);
		City pozsony = new City();
		pozsony.setName("Pozsony");
		pozsony.setLikelihoods(likelihoods);
		pozsony.setFirePowers(firePower);
		City praga = new City();
		praga.setName("Praga");
		praga.setLikelihoods(likelihoods);
		praga.setFirePowers(firePower);
		cities.add(becs);
		cities.add(budapest);
		cities.add(pozsony);
		cities.add(praga);
		
		Plane plane1 = new Plane();
		plane1.setHp(200);
		plane1.setName("Charity");
		plane1.setType(PlaneType.CHARITY);
		
		Plane plane2 = new Plane();
		plane2.setHp(200);
		plane2.setName("F11");
		plane2.setType(PlaneType.MILITARY);
		
		Plane plane3 = new Plane();
		plane3.setHp(200);
		plane3.setName("A310");
		plane3.setType(PlaneType.CIVIL);
		
		planes.add(plane1);
		planes.add(plane2);
		planes.add(plane3);
		
	}
	public void addCity(final City city) {
		cities.add(city);
	}
	
	public City getCityByName(final String name) {
		City result = null;
		for (City c : cities) {
			if (name.equals(c.getName())) {
				result = c;
			}
		}
		return result;
	}
	
	public List<City> getCities() {
		return Collections.unmodifiableList(cities);
	}
	
	public void addPlane(final Plane plane) {
		planes.add(plane);
	}
	
	public Plane getPlaneByName(final String name) {
		Plane result = null;
		for (Plane p : planes) {
			if (name.equals(p.getName())) {
				result = p;
			}
		}
		return result;
	}
	
	public List<Plane> getPlanes() {
		return Collections.unmodifiableList(planes);
	}
	
	public void addRoute(final Route Route) {
		routes.add(Route);
	}
	
	public Route getRouteByName(final String name) {
		Route result = null;
		for (Route p : routes) {
			if (name.equals(p.getName())) {
				result = p;
			}
		}
		return result;
	}
	
	public List<Route> getRoutes() {
		return Collections.unmodifiableList(routes);
	}
}
