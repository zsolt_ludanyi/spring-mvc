<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Plane Shootout - Add City</title>
	<link href="<c:url value="/resources/reset.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
	<h1>Plane Shootout - Add City</h1>
	
	<br>
	<form role="form" action="/smvc/planes/addRoute?step=routeAttributes" method="POST">
		<select name="enRouteCities">		
			<c:forEach var="pt" items="${cities}">
				<option value="${pt.name}">${pt.name}</option>	
			</c:forEach>
		</select>
		<br>
		<button type="submit"> Add City to Route</button>
	</form>
	
	<br>
	
	<form role="form" action="/smvc/planes/addRoute?step=done" method="POST">
		<button type="submit">FINALIZE ROUTE</button>
	</form>
</body>
</html>
