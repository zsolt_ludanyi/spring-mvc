<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"  %>

<html>
<head>
	<title>Plane Shootout</title>
	<!-- <link rel="stylesheet" type="text/css" href="/resources/reset.css">
    <link rel="stylesheet" type="text/css" href="/resources/style.css">-->
    <link href="<c:url value="/resources/reset.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/style.css" />" rel="stylesheet">
</head>
<body>
	<h1>Plane Shootout</h1>	
	<br>
	<table>
		<tr>
			<td>	
				<h2>Cities</h2>
				<table>
					<tr>
						<th>Name</th>
					</tr>
					<c:forEach var="c" items="${cities}">
						<tr>
							<td>${c.name}</td>
						</tr>
					</c:forEach>
				</table>
			</td>
			<td>	
				<h2>Planes</h2>
				<table>
					<tr>
						<th>Name</th>
						<th>Type</th>
						<th>HP</th>
					</tr>
					<c:forEach var="p" items="${planes}">
						<tr>
							<td>${p.name}</td>
							<td>${p.type}</td>
							<td>${p.hp}</td>
						</tr>
					</c:forEach>
				</table>
			</td>
			<td>	
				<h2>Routes</h2>
				<table>
					<tr>
						<th>Name</th>
						<th>Start</th>
						<th>Goal</th>
					</tr>
					<c:forEach var="r" items="${routes}">
						<tr>
							<td>${r.name}</td>
							<td>${r.start.name}</td>
							<td>${r.goal.name}</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
		<tr>
			<td>	
				<form role="form" action="/smvc/planes/addCity?step=cityAttributes" method="POST">
					<input name="name" type="text" value="${sessionScope.cityForm.name}" /> <br>
					<br>
					<button type="submit">ADD CITY</button>
				</form>
			</td>
			<td>	
				<form role="form" action="/smvc/planes/addPlane" method="POST">
					<input name="name" type="text" value="${planeForm.name}" /> <br>
					<select name="type">
						<c:forEach var="pt" items="${planeTypes}">
							<option value="${pt}">${pt}</option>
						</c:forEach>
					</select>
					<input name="hp" type="number" value="${planeForm.hp}" /> <br>
					<br>
					<button type="submit">ADD PLANE</button>
				</form>
			</td>
			<td>	
				<form role="form" action="/smvc/planes/addRoute?step=routeAttributes" method="POST">
					<input name="name" type="text" value="${routeForm.name}" /> <br>
					<select name="start">		
						<c:forEach var="pt" items="${cities}">
							<option value="${pt.name}">${pt.name}</option>	
						</c:forEach>
					</select>
					<select name="goal">		
						<c:forEach var="pt" items="${cities}">
							<option value="${pt.name}">${pt.name}</option>	
						</c:forEach>
					</select>
					<br>
					<button type="submit">ADD CITY TO ROUTE</button>
				</form>
			</td>
		</tr>
	</table>

</body>
</html>
